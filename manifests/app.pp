exec { "apt-get update":
  path => "/usr/bin",
}

package { "curl"
  ensure  => present,
  require => Exec['apt-get update'],
}

# Setup web server
package { "nginx-full":
  ensure  => present,
  require => Exec["apt-get update"],
}

package { "phpmyadmin":
  ensure  => present,
  require => Exec["apt-get update"],
}

service { "nginx":
  ensure  => "running",
  require => [
    Package["nginx-full"],
  ],
}

file { "nginx.conf":
  ensure => 'file',
  mode  => 644,
  owner => 'www-data',
  group => 'www-data',
  path => '/etc/nginx/nginx.conf',
  source => "/vagrant/modules/nginx/files/nginx.conf",
  notify => Service['nginx'],
  require => Package['nginx-full'],
}

file { "fastcgi_params":
  ensure => 'file',
  mode  => 644,
  owner => 'www-data',
  group => 'www-data',
  path => '/etc/nginx/fastcgi_params',
  source => "/vagrant/modules/nginx/files/fastcgi_params",
  notify => Service['nginx'],
  require => Package['nginx-full'],
}

file { 'default-nginx-disable':
  path => '/etc/nginx/sites-enabled/default',
  ensure => absent,
  notify => Service['nginx'],
  require => Package['nginx-full'],
}

file { "site-nginx-vhost":
  ensure => 'file',
  mode  => 644,
  owner => 'www-data',
  group => 'www-data',
  path => '/etc/nginx/sites-available/site',
  source => "/vagrant/modules/nginx/files/site",
  notify => Service['nginx'],
  require => Package['nginx-full'],
}

file { 'site-nginx-enable':
  path => '/etc/nginx/sites-enabled/site',
  target => '/etc/nginx/sites-available/site',
  ensure => link,
  notify => Service['nginx'],
  require => [
    Package['nginx-full'],
    File['nginx.conf'],
    File['site-nginx-vhost'],
    File['default-nginx-disable'],
  ],
}

# Setup database
class { 'mysql': }

class { 'mysql::server':
  config_hash => {
    'root_password' => 'root',
  }
}

mysql::db { 'site':
  user     => 'site',
  password => 'site',
  host     => 'localhost',
  grant    => ['all'],
}

# Setup php fpm
package { "git-core":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-ssh2":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-gd":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-curl":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-mysql":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-mcrypt":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-cli":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-dev":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-memcached":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-xdebug":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-xsl":
  ensure  => present,
  require => Exec["apt-get update"],
}
package { "php5-fpm":
  ensure  => present,
  require => [
    Exec["apt-get update"],
    Package['php5-gd', 'php5-curl', 'php5-mysql', 'php5-xsl', 'php5-mcrypt', 'php5-xdebug', 'php5-dev', 'php5-ssh2']
  ],
}

service { "php5-fpm":
  ensure  => "running",
  require => Package["php5-fpm"],
}

file { "/var/log/php-fpm":
  mode  => 644,
  owner => 'www-data',
  group => 'www-data',
  ensure => 'directory',
  require => Package['php5-fpm'],
  notify => Service['php5-fpm'],
}

file { "/etc/php5/fpm/pool.d/www.conf":
  ensure => 'file',
  mode  => 644,
  owner => 'www-data',
  group => 'www-data',
  source => "/vagrant/modules/php5-fpm/templates/www.conf",
  require => Package['php5-fpm'],
  notify => Service['php5-fpm'],
}
