# Vagrant Linux Nginx MySQL PHP environment

This a vagrant environemnt used for single projects. Meaning this should be
used for running single sites. [Vagrant](http://www.vagrantup.com/) is a
set of wrapper scripts for managing and provisioning virtualbox virtual machines.

## HOWTO

* Install [Virtualbox](https://www.virtualbox.org/wiki/Downloads) for your Operating System.
* Install the [Oracle VM Extension Pack](https://www.virtualbox.org/wiki/Downloads).
* Install [Vagrant](http://downloads.vagrantup.com/).
* Create Project folder structure. In this example we are setting a site up for the gear site.

        Projects
            - gear
                - site
                    - wordpress
                        - core
                        - wp-config.php
                        - wp-content

* Within the gear folder clone this repository into a folder named vagrant

        git clone -b master git@bitbucket.org:bgreenacre42/vagrant-lnmpstack.git vagrant

* Now that you have a clone copy it is a good idea to rename the remote to something not origin.

        git remote rename origin upstream

* Boot up your vagrant.

        cd Projects/gear/vagrant
        
        # On the first boot the provisioner can take several minutes to finish booting
        # this is because it's installing and configuring all the required services,
        # packages and resources to run your site.
        vagrant up

* Shutting down your vagrant

        cd Projects/gear/vagrant
        vagrant halt

* Deleting your vagrant

        cd Projects/gear/vagrant
        vagrant destroy

## FAQ

Q. How can I add another nginx host?

A. Don't. This vagrant configuration is meant for single site setups.

Q. Vagrant is reporting a port collision on 8080. How can I fix this?

A. Open the Vagrantfile and change "config.vm.network :forwarded_port, guest: 80, host: 8080" to another port number.

Q. I want to use a host name rather than localhost:8080 to get to my site. How can I do that with vagrant?

A. Open the Vagrantfile and uncomment config.vm.network :private_network, ip: "192.168.36.10" and change the IP to something unique for your computer. Use this IP and add it to your /etc/hosts file with the host name you want to use.
